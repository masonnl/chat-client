/******************************************
 * ------------[ Mason Neal ]------------ *
 * [ CS-140: Networks ]                   *
 * TCP chat client using readline         *
 * References:                            *
 *    - Connor Riva                       *
 *    - Nolan Blew                        *
 *    - Thomas Cantrell                   *
 ******************************************
 *****************************************/
#include <errno.h>
#include <stdio.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <readline/readline.h>

#define SERVER_PORT 49153
#define SERVER "localhost"
#define CLIENT_SERVER_BUFFER 1024

static char local_buffer[CLIENT_SERVER_BUFFER+1];
char *prompt = "> ";
int sock;

/* Catch error messages */
void error(char *msg)
{
    perror(msg);
    exit(0);
}

/* Catch any client commands */
void help(char* line)
{
    if (strcmp(line,".q") == 0 || strcmp(line,".quit") == 0)
    {
        printf("\nClosing...\n");
        exit(0);
    }
    else if (strcmp(line,".h") == 0 || strcmp(line,".help") == 0 || strcmp(line,".?") == 0)
    {
        printf("\nHELP for TCP chat client using enhanced I/O:\n");
        printf("--\t.q\tquit the client\n");
        printf("--\t.h\tOpen help, such as it is\n");
        printf("For more usage information type '.usage'\n");
    }
    else if (strcmp(line,".usage") == 0)
    {
        printf("\nUSAGE: [HOST]\n");
        printf("This client will automatically default to a localhost connection on port 49153\n");
        printf("-- If you would like to run this client on a different host, provide it as the first argument\n");
        printf("-- If you would like to change the port, provide the port number as the second argument.\n");
    }
}

/* Handle_line is called at each installment of readline */
void handle_line(char* line)
{
    help(line);
    if (strlen(line) != 0)
    {
        strcat(line, "\n");
        if (write(sock, line, strlen(line)) < 0)
            printf("\nError writing to server.\n");
        free(line);
    }
}

void main()
{
    int ret;
    fd_set readfd;
    fd_set readcpyfd;
    struct hostent *server;
    struct sockaddr_in servaddr;

    if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    { // Check for errors in the socket constructor
        error("ERROR constructing socket");
    }

    /* Construct the server */
    server = gethostbyname(SERVER);
    if (server == NULL)
    {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }

    /* Zero out the servaddr */
    bzero((char *) &servaddr, sizeof(servaddr));
    servaddr.sin_family = AF_INET;

    /* Copy host address to servaddr */
    bcopy((char *)server->h_addr, (char *)&servaddr.sin_addr.s_addr, server->h_length);
    servaddr.sin_port = htons(SERVER_PORT);

    /* Establish a connection to the server */
    if (connect (sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)
        error("Had an error while connecting to the server");

    /* Install the callback before setting fd's */
    rl_callback_handler_install(prompt, handle_line);
    FD_ZERO(&readfd);
    FD_ZERO(&readcpyfd);
    FD_SET(sock,&readfd);
    FD_SET(fileno(rl_instream),&readfd);

    while(1)
    {
        memcpy(&readcpyfd,&readfd,sizeof(&readcpyfd));
        ret = select(sock+1,&readcpyfd,NULL,NULL,NULL);

        if(ret==-1)
            error("Select returned an error");

        /* Receive from server */
        if (FD_ISSET(sock,&readcpyfd))
        {
            ret = read(sock,local_buffer,CLIENT_SERVER_BUFFER);
            if(ret==-1)
            {
                error("error receiving.");
            }
            else if (ret == 0)
            {
                printf("Exiting...\n");
                exit(0);
            }
            else
            {
                local_buffer[ret] = 0;
                rl_save_prompt();
                rl_message(local_buffer);
                rl_clear_message();
                rl_restore_prompt();
            }
        } // end recieve if

        /* Write to the server */
        if(FD_ISSET(fileno(rl_instream), &readcpyfd))
            rl_callback_read_char();

    } // end while
} // end main
