#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>

#define PORT 49153
#define MAX_SIZE 1024

/* Struct for handling user connections */
struct Connection
{
    int fd;               // client's file descriptor
    char name[MAX_SIZE];  // client's name (init to '\0')
};

/* Declarations */
/*******************************************/
int listen_desc, conn_desc;                 // main listening descriptor and connected descriptor
int maxfd, maxi;                            // max value descriptor and index in client array
int i,j,k,b;                                // loop variables
int yes;                                    // port blocking switch
int numready;                               // select variable
fd_set tempset, savedset;                   // descriptor set to be monitored
struct Connection client[FD_SETSIZE];       // array of client descriptors
struct sockaddr_in serv_addr, client_addr;  // sockaddr inits
char buff[MAX_SIZE];                        // main I/O buffer
char msg[MAX_SIZE];                         // broadcast buffer
/*******************************************/

/* Catch error messages */
void error(char *msg)
{
    perror(msg);
    exit(0);
}

/* Strip a newline from a string in memory */
void strip_newline()
{
    int new_line = strlen(buff) -1;
    if (buff[new_line] == '\n')
        buff[new_line] = '\0';
}

/* Broadcast a message to all clients */
void broadcast(char* msg)
{
    for(b=0; b<=maxi+1; b++)
    {
        write(client[b].fd,msg,strlen(msg));
    }
}

/* Handle data from a client */
void receive()
{
    strip_newline();
    if (client[k].name[0] == '\0' || client[k].name == "")
    {
        strcpy(client[k].name,buff);
        strcat(msg, buff);
        strcat(msg, " has joined the room\n");
        broadcast(msg);
    }
    else
    {
        strcat(msg, client[k].name);
        strcat(msg, ": ");
        strcat(msg, buff);
        strcat(msg, "\n");
        broadcast(msg);
    }
}

/* Go forth */
int main()
{
    listen_desc = socket(AF_INET, SOCK_STREAM, 0);

    if(listen_desc < 0)
        error("Failed creating socket\n");

    bzero((char *)&serv_addr, sizeof(serv_addr));

    /* Allow binding to be disconnected/reconnected quickly (ctrl+c) */
    if(setsockopt(listen_desc, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        error("Server setsockopt() error");

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PORT);

    if (bind(listen_desc, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
        error("Failed to bind");

    listen(listen_desc, 5);

    maxfd = listen_desc; // Initialize the max descriptor with the first valid one we have
    maxi = -1; // index in the client connected descriptor array
    for (i=0; i<FD_SETSIZE; i++)
        client[i].fd = -1;  // Indicate that the descriptor is "empty"
    FD_ZERO(&savedset);
    FD_SET(listen_desc, &savedset); // add the current listening descriptor to the monitored set

    /* Main listening loop */
    while(1)
    {
        bzero(buff, MAX_SIZE);
        bzero(msg, MAX_SIZE);
        tempset = savedset;
        numready = select(maxfd+1, &tempset, NULL, NULL, NULL);

        /* Check for a knock at the door (new client) */
        if(FD_ISSET(listen_desc, &tempset))
        {
            printf("new client connection\n");
            int size = sizeof(client_addr);
            conn_desc = accept(listen_desc, (struct sockaddr *)&client_addr, &size);
            for (j=0; j<FD_SETSIZE; j++)
                if(client[j].fd < 0)
                {
                    client[j].fd = conn_desc; // save the fd
                    break;
                }

            /* Add it to the set of monitored descriptors */
            FD_SET(conn_desc, &savedset);
            if(conn_desc > maxfd)
                maxfd = conn_desc; // max for select
            if(j > maxi)
                maxi = j;   // max index in client array
        } // end new connection

        /* Check if there is any data from the clients */
        for(k=0; k<=maxi; k++)
        {
            if(client[k].fd > 0)
            {
                if(FD_ISSET(client[k].fd, &tempset))
                {
                    int num_bytes;
                    if( (num_bytes = read(client[k].fd, buff, MAX_SIZE)) > 0)
                        receive(num_bytes);

                    if(num_bytes == 0)  // connection was closed by client
                    {
                        strcat(msg, client[k].name);
                        strcat(msg, " has left the room\n");
                        broadcast(msg);
                        close(client[k].fd);
                        FD_CLR(client[k].fd, &savedset);
                        client[k].fd = -1;
                        strcpy(client[k].name, "");
                    }
                }
            }
        } // end client data handling
    } // end listening loop

    close(listen_desc);
    return 0;
}
